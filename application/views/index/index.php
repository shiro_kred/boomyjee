<?php defined('SYSPATH') or die('No direct script access.'); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= isset($description) ? $description : '' ?>">

    <title><?= isset($title) ? $title : '' ?></title>

    <link href="/media/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/media/base/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="main">
<div class="col-lg-12">
    <div class="col-lg-6 col-lg-offset-3 reply">
        <h1>Написать отзыв</h1>
        <form id="replyForm" class="col-lg-12" method="post" role="form" data-toggle="validator">
            <div class="col-lg-8">
                <div class="form-group">
                    <label for="emailField">Email</label>
                    <input name="email" type="email" class="form-control" id="emailField" placeholder="Email"
                           data-error="Укажите коректный E-mail" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label for="textField">Текст</label>
                    <textarea name="text" id="textField" class="form-control" cols="30" rows="10"
                              placeholder="Текст вашего отзыва" style="max-width: 100%"></textarea>
                </div>
                <a id="replyFormBtn" class="btn btn-default">Отправить</a>

            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="exampleInputFile">Добавить изображение</label>
                    <input type="file" id="photo">
                    <div>
                        <ul id="preview-photo">
                        </ul>
                    </div>
                    <p class="help-block">Выможете добавить изображение (оно будет ужато до 320px)</p>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Предварительный просмотрм
                    </label>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-6 col-lg-offset-3 reply-list">
        <table class="table table-hover col-lg-12">
            <thead>
                <tr>
                    <td class="element-reply-sort col-lg-2" data-sort="photo">Photo</td>
                    <td class="element-reply-sort col-lg-2" data-sort="email">Email</td>
                    <td class="element-reply-sort col-lg-6" data-sort="text">Text</td>
                    <td class="element-reply-sort col-lg-2" data-sort="create">Date</td>
                </tr>
            </thead>
            <tbody id="loader-reply">
            </tbody>
        </table>
    </div>
</div>
<script id="template-loader-reply" type="text/template">
    <tr class="item-reply">
        <td><%=Photo%></td>
        <td><%=Email%></td>
        <td><%=Text%></td>
        <td><%=Date%></td>
    </tr>
</script>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/media/libs/bootstrap/js/bootstrap.js"></script>
<script src="/media/libs/bootstrap/js/validator.js"></script>
<script src="/media/base/js/libs/underscore/underscore-min.js" type="text/javascript"></script>
<script src="/media/base/js/libs/backbone/backbone.min.js" type="text/javascript"></script>
<script src="/media/base/app/app.js" type="text/javascript"></script>
<script src="/media/base/app/reply/index/router_search.js" type="text/javascript"></script>
<script src="/media/base/app/reply/index/view_main.js" type="text/javascript"></script>
<script>
    var previewWidth = 320, // ширина превью
        previewHeight = 320, // высота превью
        maxFileSize = 2 * 1024 * 1024, // (байт) Максимальный размер файла (2мб)
        selectedFiles = {},// объект, в котором будут храниться выбранные файлы
        queue = [],
        image = new Image(),
        imgLoadHandler,
        isProcessing = false,
        errorMsg, // сообщение об ошибке при валидации файла
        previewPhotoContainer = document.querySelector('#preview-photo'); // контейнер, в котором будут отображаться превью

    // Когда пользователь выбрал файлы, обрабатываем их
    $('input[type=file][id=photo]').on('change', function() {
        var newFiles = $(this)[0].files; // массив с выбранными файлами

        for (var i = 0; i < newFiles.length; i++) {

            var file = newFiles[i];

            // В качестве "ключей" в объекте selectedFiles используем названия файлов
            // чтобы пользователь не мог добавлять один и тот же файл
            // Если файл с текущим названием уже существует в массиве, переходим к следующему файлу
            if (selectedFiles[file.name] != undefined) continue;

            // Валидация файлов (проверяем формат и размер)
            if ( errorMsg = validateFile(file) ) {
                alert(errorMsg);
                return;
            }

            // Добавляем файл в объект selectedFiles
            selectedFiles[file.name] = file;
            queue.push(file);

        }

        $(this).val('');
        processQueue(); // запускаем процесс создания миниатюр
    });

    // Валидация выбранного файла (формат, размер)
    var validateFile = function(file)
    {
        if ( !file.type.match(/image\/(jpeg|jpg|png|gif)/) ) {
            return 'Фотография должна быть в формате jpg, png или gif';
        }

        if ( file.size > maxFileSize ) {
            return 'Размер фотографии не должен превышать 2 Мб';
        }
    };

    var listen = function(element, event, fn) {
        return element.addEventListener(event, fn, false);
    };

    // Создание миниатюры
    var processQueue = function()
    {
        // Миниатюры будут создаваться поочередно
        // чтобы в один момент времени не происходило создание нескольких миниатюр
        // проверяем запущен ли процесс
        if (isProcessing) { return; }

        // Если файлы в очереди закончились, завершаем процесс
        if (queue.length == 0) {
            isProcessing = false;
            return;
        }

        isProcessing = true;

        var file = queue.pop(); // Берем один файл из очереди

        var li = document.createElement('LI');
        var span = document.createElement('SPAN');
        var spanDel = document.createElement('SPAN');
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');

        span.setAttribute('class', 'img');
        spanDel.setAttribute('class', 'delete');
        spanDel.innerHTML = 'Удалить';

        li.appendChild(span);
        li.appendChild(spanDel);
        li.setAttribute('data-id', file.name);

        image.removeEventListener('load', imgLoadHandler, false);

        // создаем миниатюру
        imgLoadHandler = function() {
            ctx.drawImage(image, 0, 0, previewWidth, previewHeight);
            URL.revokeObjectURL(image.src);
            span.appendChild(canvas);
            isProcessing = false;
            setTimeout(processQueue, 320); // запускаем процесс создания миниатюры для следующего изображения
        };

        // Выводим миниатюру в контейнере previewPhotoContainer
        previewPhotoContainer.appendChild(li);
        listen(image, 'load', imgLoadHandler);
        image.src = URL.createObjectURL(file);

        // Сохраняем содержимое оригинального файла в base64 в отдельном поле формы
        // чтобы при отправке формы файл был передан на сервер
        var fr = new FileReader();
        fr.readAsDataURL(file);
        fr.onload = (function (file) {
            return function (e) {
                $('#preview-photo').append(
                    '<input type="hidden" name="photos[]" value="' + e.target.result + '" data-id="' + file.name+ '">'
                );
            }
        }) (file);
    };

    // Удаление фотографии
    $(document).on('click', '#preview-photo li span.delete', function() {
        var fileId = $(this).parents('li').attr('data-id');

        if (selectedFiles[fileId] != undefined) delete selectedFiles[fileId]; // Удаляем файл из объекта selectedFiles
        $(this).parents('li').remove(); // Удаляем превью
        $('input[name^=photo][data-id="' + fileId + '"]').remove(); // Удаляем поле с содержимым файла
    });

</script>
</body>
</html>
