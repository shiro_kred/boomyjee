<?php defined('SYSPATH') or die('No direct script access.'); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= isset($description) ? $description : '' ?>">

    <title><?= isset($title) ? $title : '' ?></title>
</head>
<body>
<?= isset($content) ? $content : ''?>
</body>
</html>
