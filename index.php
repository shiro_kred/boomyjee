<?php

$application = 'application';
$views = 'views';
$modules = 'modules';
$system = 'system';

define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('EXT', '.php');

if ( ! is_dir($application) AND is_dir(DOCROOT.$application))
    $application = DOCROOT.$application;
if ( ! is_dir($modules) AND is_dir(DOCROOT.$modules))
    $modules = DOCROOT.$modules;
if ( ! is_dir($system) AND is_dir(DOCROOT.$system))
    $system = DOCROOT.$system;
if ( ! is_dir($views) AND is_dir(DOCROOT.$views))
    $views = DOCROOT.$views;

define('APPPATH', realpath($application).DIRECTORY_SEPARATOR);
define('MODPATH', realpath($modules).DIRECTORY_SEPARATOR);
define('SYSPATH', realpath($system).DIRECTORY_SEPARATOR);
define('VIEPATH', realpath($views).DIRECTORY_SEPARATOR);

unset($application, $modules, $system, $views);

// Application
require APPPATH.'app'.EXT;
require SYSPATH.'core/core'.EXT;
require SYSPATH.'core/route'.EXT;
require SYSPATH.'core/database'.EXT;

// Base Class
require SYSPATH.'controller/base_api'.EXT;
require SYSPATH.'controller/base_action'.EXT;
require SYSPATH.'helper/text'.EXT;
require APPPATH.'models/reply'.EXT;

Core::init();

