var replyIndexMain = Backbone.View.extend({

    el: '#main',

    template: _.template($("#template-loader-reply").html()),

    events: {
        'click #replyFormBtn': 'replyFormBtnClick',
        'click .element-reply-sort': 'sortReply'
    },

    initialize: function () {
        console.log('init... main...');

        this.form = this.$('#replyForm');
        $('#replyForm').validator();

        this.emailField = this.$("#emailField");
        this.textField = this.$("#textField");

        this.sort = '';
        this.SortBy = 'ASK';
        this.loader = this.$("#loader-reply");
        this.loaderReply();
    },

    replyFormBtnClick: function (e) {
        e.preventDefault();

        this.dataField = {
            email: this.emailField.val(),
            text: this.textField.val(),
            photo: $("input[name='photos[]']").val()
        };

        console.log(this.dataField);

        var url = window.location.origin + '/api/reply';
        var that = this;
        $.post(url, this.dataField, function (response) {
            try {
                if (response.status == true) {
                    console.log('true');
                    alert("Выш отзыва отправлен на модерацию");
                } else {
                    console.log("Error");
                }
            } catch (error) {
                console.log("Error");
            }
        });
    },

    loaderReply: function () {
        var url = window.location.origin + '/api/getList' + this.sort;
        var that = this;
        $.post(url, this.dataField, function (response) {
            try {
                if (response.status == true) {
                    _.each(response.data, function (value, key) {
                        var photo = '';
                        if(value['photo']) {
                            photo = "<img src='/media/image/" + value['photo'] +"'>";
                        }
                        that.loader.append(that.template({
                            Photo: photo,
                            Email: value['email'],
                            Text: value['text'],
                            Date: value['create'],
                        }));
                    });

                } else {
                    console.log("Error");
                }
            } catch (error) {
                console.log("Error");
            }
        });
    },

    sortReply: function (e) {
        e.preventDefault();
        console.log(e.toElement.getAttribute('data-sort'));
        if(this.SortBy == 'ASK') {
            this.SortBy = 'DESC';
        } else if (this.SortBy == 'DESC') {
            this.SortBy = 'ASK';
        }

        this.sort = '?order_by=' + e.toElement.getAttribute('data-sort') + '&sort=' + this.SortBy;
        this.loader.empty();
        this.loaderReply();
    }

});

window.shiro_kred.Views.replyIndexMain = new replyIndexMain();