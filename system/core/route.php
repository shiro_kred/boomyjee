<?php defined('SYSPATH') or die('No direct access allowed.');

class Route
{
    public $path;
    
    public function setPath($val) {
        $this->path = $val;
    }
    
    public function getPath() {
        return $this->path;
    }
    
    /**
     * Project initialization
     * @param array|NULL $settings
     */
    public static function init(array $settings = NULL)
    {
        $url = $_GET['url'];
        $url = rtrim($url, '/');
        $url = explode('/', $url);

        if(!$url[0]) {
            $url[0] = 'index';
        }

        require APPPATH.'controllers'.DIRECTORY_SEPARATOR.$url[0].EXT;

        $controller = new $url[0];
        $action = $url[1];
        $param = $url[2];
        $GLOBALS['param'] = $url[2];

        if (isset($param)) {
            $controller->$action($param);

        } else {
            if (isset($action)) {
                $controller->$action();
            }
        }
    }
}