<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
    'default' => array
    (
        'type' => 'MySQL',
        'connection' => array(
            'hostname'   => 'localhost',
            'database'   => 'boomyjee',
            'username'   => 'root',
            'password'   => 'root',
        ),
        'table_prefix' => false,
        'charset'      => 'utf8',
        #'caching'      => true,
        #'profiling'    => true,
    )
);
