<?php defined('SYSPATH') or die('No direct access allowed.');

class base_action {

    public $action, $template, $file, $title;

    public function View($file, $data = null)
    {
        if(is_array($data)) {
            extract($data);
        }

        $this->file = APPPATH.$file.EXT;
        $this->template = include ($this->file);
    }
}