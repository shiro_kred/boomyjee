<?php defined('SYSPATH') or die('No direct access allowed.');

class base_api {

    public $status = false, $message, $data, $query;

    public function response()
    {
        $result = [
            'status'    => $this->status,
            'message'   => $this->message,
            'data'      => $this->data,
            'query'     => $this->query
        ];
        header('Content-Type: application/json');
        echo (json_encode($result));
    }
    
    public function DB($where, $is_array = true) {
        $DB = new Database();
        $DB->query($where , $is_array);
    }
}